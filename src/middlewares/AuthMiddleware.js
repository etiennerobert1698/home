import {AuthProvider} from "../providers/AuthProvider";

export const AuthMiddleware = {
    async isAuth(req, res, next) {
        try {
            const token = AuthProvider.getTokenFromHeader(req);
            if(token) {
                const auth = await AuthProvider.isAuth(token);
                if (auth) {
                    req.auth = auth;
                    next();
                } else {
                    res.status(401).json({status: "error", message: "Unauthorized"});
                }
            } else {
                res.status(401).json({status: "error", message: "Unauthorized"});
            }
        } catch(err) {
            console.log(err);
            res.status(401).json({status: "error", message: "Unauthorized"});
        }

    }

};
