const Sequelize = require('sequelize');
import {config} from "../config/config";

import User from './User';
import Message from './Message';

const sequelize = new Sequelize(config.db, {
    define: {
        // The `timestamps` field specify whether or not the `createdAt` and `updatedAt` fields will be created.
        // This was true by default, but now is false by default
        timestamps: false
    }
});

const models = {
    User: User.init(sequelize, Sequelize),
    Message: Message.init(sequelize, Sequelize),
};

// Run `.associate` if it exists,
// ie create relationships in the ORM
Object.values(models)
    .filter(model => typeof model.associate === 'function')
    .forEach(model => model.associate(models));

const db = {
    ...models,
    sequelize,
};

module.exports = db;
