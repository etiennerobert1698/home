const Sequelize = require("sequelize");


export default class User extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init({
                name: DataTypes.STRING,
                firstname: DataTypes.STRING,
                email: DataTypes.STRING,
                passwd: DataTypes.STRING
            }, {sequelize, tableName: "users"}
        );
    }

    static async findByEmail(email) {
        return await User.findOne({where: {email: email}});
    }

    static checkEmail(email) {
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    }

    toJSON() {
        var values = Object.assign({}, this.get());
        delete values.passwd;
        return values;

    }


}
