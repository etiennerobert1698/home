import User from "./User";
import * as bcrypt from "bcrypt";

const Sequelize = require("sequelize");
const Op = Sequelize.Op;
export default class Message extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init({
                senderId: {
                    type: DataTypes.INTEGER,
                    allowNull: false,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                recipientId: {
                    type: DataTypes.INTEGER,
                    allowNull: false,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                message: DataTypes.STRING,
                time: DataTypes.DATE
            }, {sequelize, tableName: "messages"}
        );
    }


    static async findConversation(authId, recipientId, untilTime = Date.now(), limite = 10) {
        return Message.findAll({
            where: {
                [Op.and]: [
                    {
                        time: {
                            [Op.lt]: untilTime
                        }
                    },
                    {
                        [Op.or]: [

                            {
                                [Op.and]: [
                                    {senderId: recipientId},
                                    {recipientId: authId}
                                ],

                            },
                            {
                                [Op.and]: [
                                    {senderId: authId},
                                    {recipientId: recipientId}
                                ]
                            }

                        ]
                    }
                ]
            },
            limit: limite,
            order: [
                ['time', 'DESC']
            ]
        });
    }

    static saveMessage(authId, recipientId, message) {
        Message.create({
            senderId: authId,
            recipientId: recipientId,
            message: message,
            time: Date.now()
        });
    }
}
