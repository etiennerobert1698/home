import {config} from "../config/config";
import User from "../models/User";
const jwt = require('jsonwebtoken');

export const AuthProvider = {
    getTokenFromHeader(req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        }
        return false;
    },

    async isAuth(token) {
        try {
            let decoded = jwt.verify(token, config.jwt.jwt_secret);
            const auth = await User.findByPk(decoded.id);
            if (auth) {
                return auth;
            }
        } catch (err) {
            return false;
        }
    }
};
