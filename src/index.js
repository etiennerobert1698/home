import express from 'express';
//controllers
import {UserController} from './controllers/UserController';
import {AuthController} from "./controllers/AuthController";
import {MessageController} from "./controllers/MessageController";
//middlewares
import {AuthMiddleware} from "./middlewares/AuthMiddleware";
//lib
let app = express();
let http = require('http').createServer(app);
let io = require('socket.io')(http);
let path = require('path');
//sockets
require("./controllers/sockets/Chat")(io);

//init sequelize models
require("./models/sequelize");

//middleware
app.use(express.json());
app.use('/users', (req, res, next) => {
    AuthMiddleware.isAuth(req, res, next)
});
app.use('/messages', (req, res, next) => {
    AuthMiddleware.isAuth(req, res, next)
});

//template
app.route('/').get((req, res) => {
    res.sendFile(path.join(__dirname, 'templates/basic/index.html'));
});

//controllers
app.route('/register').post((req, res) => {
    AuthController.register(req, res);
});
app.route('/login').post((req, res) => {
    AuthController.login(req, res)
});


app.route('/users')
    .get((req, res) => {
        UserController.findAll(req, res);
    });
app.route('/messages')
    .get((req, res) => {
        MessageController.findAll(req, res);
    });


//error handler
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send({status: "error", message: "Something broke"});
});

http.listen(8080);
