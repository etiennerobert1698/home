import User from '../models/User';


export const UserController = {
    async findAll(req, res) {
        try {
            const users = await User.findAll({attributes: ["id", "email", "name", "firstname"]});
            res.status(200).json({status: 'success', users: users});

        } catch (err) {
            res.status(500).json({status: 'error'});

        }
    }
};
