import User from '../models/User';
import * as bcrypt from 'bcrypt';
import { config } from '../config/config'
const jwt = require('jsonwebtoken');

export const AuthController = {
    async register(req, res) {
        if (req.body.email && req.body.passwd && req.body.name && req.body.firstname) {
            if (User.checkEmail(req.body.email)) {
                const user = await User.create({
                    firstname: req.body.firstname,
                    name: req.body.name,
                    email: req.body.email,
                    passwd: await bcrypt.hash(req.body.passwd, 8)
                });
                res.status(200).json({status: "success", user: user});
            } else {
                res.status(400).json({status: "error", message: "Email is not valid"});
            }
        } else {
            res.status(400).json({status: "error", message: "Fields required"});
        }

    },
    generateJWT(id) {
        const data = {
            id: id,
        };
        return jwt.sign(data, config.jwt.jwt_secret, { expiresIn: config.jwt.validityTime });
    },

    async login(req, res) {
        if (req.body.email && req.body.passwd) {
            const userRecord = await User.findByEmail(req.body.email);
            if (userRecord) {
                const testPasswd = await bcrypt.compare(req.body.passwd, userRecord.passwd);
                if (testPasswd) {
                    res.status(200).json({status: "success", user: userRecord, token: this.generateJWT(userRecord.id)});
                } else {
                    res.status(400).json({status: "error", message: "Email or password not valid"});
                }

            } else {
                res.status(400).json({status: "error", message: "Email or password not valid"});
            }
        } else {
            res.status(400).json({status: "error", message: "Fields required"});
        }
    }
};
