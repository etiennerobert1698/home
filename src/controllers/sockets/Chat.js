import {AuthProvider} from "../../providers/AuthProvider";
import Message from '../../models/Message'

let clients = [];
module.exports = function (io) {
    io.on('connection', async (socket) => {


        socket.on('token', async (data) => {
            if (data.token) {
                const auth = await AuthProvider.isAuth(data.token);
                if (auth) {
                    clients.push({socketId: socket.id, userId: auth.id});
                    socket.emit('token', {status: 'success', message: 'connected'});
                } else {
                    socket.emit('token', {status: 'error', message: 'bad token'});
                }
            } else {
                socket.emit('token', {status: 'error', message: 'token not provided'});
            }
        });


        socket.on('chat', async (data) => {

            if (data.message && data.recipient) {
                //todo : stocker les sockets users dans une db
                const auth = clients.find((client) => {
                    return client.socketId === socket.id;
                });
                const recipient = clients.find((client) => {
                    return client.userId == data.recipient;
                });
                if (auth) {
                    Message.saveMessage(auth.userId, data.recipient, data.message);
                    if (recipient) {
                        io.to(recipient.socketId).emit('chat', {message: data.message, senderId: auth.userId});
                    }
                }
            }
        });

        socket.on('disconnect', function () {
            let index = clients.indexOf(clients.find((client) => {
                return client.socketId === socket.id;
            }));
            if (index > -1) {
                clients.splice(index, 1);

            }
        });

    });

};
