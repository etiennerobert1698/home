import Message from '../models/Message';


export const MessageController = {
    async findAll(req, res) {
        if (req.query.recipientId) {
            try {
                let messages = await Message.findConversation(req.auth.id, req.query.recipientId, req.query.until ? req.query.until : Date.now());
                res.status(200).json({status: 'success', messages: messages});
            } catch (err) {
                res.status(500).json({status: 'error'});
            }
        } else {
            res.status(500).json({status: 'error', message: 'Fields required'});
        }

    }
};
